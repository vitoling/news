/*
Navicat MySQL Data Transfer

Source Server         : local-root
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : news

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2015-10-14 08:21:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `news_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `channel_id` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `url` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `create_time` bigint(20) DEFAULT NULL,
  `favorite_count` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`news_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66676 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('2445', '32423', '打飞机就好', '大啊了飞机吗', null, '1', '腾讯网', '201510111516', '25777');
INSERT INTO `news` VALUES ('66666', '55555', '积分 就', '公分就吧警方白玫瑰暴风科技加班费白金 ', null, '1', '新华网', '201510101515', '68768');
INSERT INTO `news` VALUES ('66667', '75', '的', '事儿让人挺好如何', null, '2', '阿迪', '4353', '324');
INSERT INTO `news` VALUES ('66668', '4', ' 会让人', '二你有ie导航敌人土豪', null, '3', '二', '46564', '3555');
INSERT INTO `news` VALUES ('66669', '5', ' 分', ' 尽快人 他国际饭店', null, '4', '额', '433575', '653552');
INSERT INTO `news` VALUES ('66670', '656', ' 的', '天然黑天然黑他', null, '1', '二', '24546', '7345');
INSERT INTO `news` VALUES ('66671', '46', ' 豆腐乳', '额特utf合同ukulele个', null, '1', '二', '547689', '35');
INSERT INTO `news` VALUES ('66672', '565', '分个', '二       个人', null, '2', '我日额', '4657', '235');
INSERT INTO `news` VALUES ('66673', '5656', '额', '是让热议话题的BTV额', null, '2', '惹宝贝', '36562', '235');
INSERT INTO `news` VALUES ('66674', '346', 'o9o', '让他爸爸他们发个吧台', null, '4', '舞台背景', '366845', '25');
INSERT INTO `news` VALUES ('66675', '4656', '问问', '我特别荣誉', null, '4', '荣誉', '5325435', '562');
