package com.news.controller;

import com.news.exception.BusinessError;
import com.news.exception.BusinessException;
import com.news.model.ArticleEntity;
import com.news.model.ChannelEntity;
import com.news.service.ArticleService;
import com.news.service.ChannelService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lingzhiyuan.
 * Date : 15/11/19.
 * Time : 下午9:09.
 * Description:
 */

@Controller
@RequestMapping("/article")
public class ArticleController {

    @Resource
    private ChannelService channelService;
    @Resource
    private ArticleService articleService;

    @RequestMapping("/append")
    public String toAppend(@RequestParam(required = false)Long column_id,Model model){

        List<ChannelEntity> channels = channelService.listChannelsAndColumns();

        model.addAttribute("column_id",column_id);
        model.addAttribute("channels",channels);
        return "article/append";

    }

    @RequestMapping("/toEdit")
    public String toUpdate(@RequestParam(required = true)Long article_id,Model model){

        List<ChannelEntity> channels = channelService.listChannelsAndColumns();
        ArticleEntity article = articleService.getById(article_id);

        model.addAttribute("article",article);
        model.addAttribute("channels",channels);
        return "article/update";

    }

    /**
     * 新闻删除
     * */
    @RequestMapping("/delete")
    @ResponseBody
    public Object deleteArticle(@RequestParam(required = true)String deleteIds){

        String[] ids;
        List deletedIds = new ArrayList<>();
        if (deleteIds != null && deleteIds.length() != 0){
            ids = deleteIds.split(",");
            for (String id : ids){
                articleService.deleteColumn(Long.valueOf(id));
                deletedIds.add(id);
            }
        }else {
            throw new BusinessException(BusinessError.ERROR_COLUMN_NOT_FOUND);
        }
        Map map = new HashMap<>();
        map.put("code",1);
        map.put("ids",deletedIds);

        return map;

    }

    /**
     * 新闻保存
     * */
    @RequestMapping("/save")
    @ResponseBody
    public Object saveNews(@RequestParam(required = false,defaultValue = "0")Long news_id,
                           @RequestParam(required = true)Long column_id,
                           @RequestParam(required = true)String title,
                           @RequestParam(required = false,defaultValue = "")String source,
                           @RequestParam(required = true)String content){

        articleService.saveOrUpdate(news_id,column_id,source,title,content);

        return null;
    }

    @RequestMapping("/list")
    public String showAll(@RequestParam(required = false,defaultValue = "0")Long column_id,
                          @RequestParam(required = false,defaultValue = "")String queryKey,
                          @RequestParam(required = false,defaultValue = "10")int page_size,
                          @RequestParam(required = false,defaultValue = "1")int page_num,
                          Model model){

        List<ChannelEntity> channels = channelService.listChannelsAndColumns();
        List<ArticleEntity> articles = null;
        long totalRow = 0;

        if (column_id != 0){
            Map result = articleService.findArticlesByColumnId(column_id,page_size,page_num);
            articles = (List<ArticleEntity>) result.get("articles");
        }else {
            articles = articleService.findAllArticlesByQueryKey(queryKey);
        }

        model.addAttribute("articles",articles);
        model.addAttribute("queryKey",queryKey);

        model.addAttribute("column_id",column_id);
        model.addAttribute("channels",channels);
        return "article/index";

    }

}
