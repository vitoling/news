package com.news.controller;

import com.news.config.Constant;
import com.news.model.ChannelEntity;
import com.news.model.ColumnEntity;
import com.news.service.ChannelService;
import com.news.service.ColumnService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/8.
 * Time : 下午9:21.
 * Description:
 */

@Controller
@RequestMapping("/channel")
public class ChannelController {

    @Resource
    private ChannelService channelService;
    @Resource
    private ColumnService columnService;

    @RequestMapping("/list")
    @ResponseBody
    public Object channelList() {

        List channels = channelService.listAllChannels();

        return channels;

    }

    /**
     * 去往添加频道的页面
     */
    @RequestMapping("/toAppend")
    public String toAppendChannel() {
        return "channel/append";
    }

    /**
     * 执行新增一个频道的操作
     */
    @RequestMapping("/append")
    @ResponseBody
    public Object appendNewChannel(@RequestParam(required = true) String title,
                                   @RequestParam(required = true) Integer type) {

        ChannelEntity channel = channelService.saveNewChannel(title, type);

        return channel;
    }

    /**
     * 执行新增一个频道的操作
     */
    @RequestMapping("/update")
    @ResponseBody
    public Object appendNewChannel(@RequestParam(required = true) Long channel_id,
                                   @RequestParam(required = true) String title,
                                   @RequestParam(required = true) Integer type) {

        ChannelEntity channel = channelService.updateChannel(channel_id, title, type);

        return channel;
    }

    /**
     * 执行删除一个频道的操作
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Object deleteChannel(@RequestParam(required = true) Long channel_id) {

        channelService.deleteChannel(channel_id);
        return "";

    }

    /**
     * 去往频道总管理页面
     */
    @RequestMapping("/setting")
    public String setting(Model model) {

        List channels = channelService.listAllChannels();

        model.addAttribute("channels", channels);
        return "channel/setting";

    }

    /**
     * 去往管理一个频道的页面
     */
    @RequestMapping("/toEdit")
    public String toEditChannel(@RequestParam(required = true) Long channel_id, Model model) {


        List channels = channelService.listAllChannels();

        ChannelEntity channel;
        channel = channelService.getChannelAndColumnsByChannelId(channel_id);
        List<ColumnEntity> columns = channel.getColumns();
        if (columns != null && columns.size() != 0) {
            for (ColumnEntity column : columns) {
                if (column.getType() != null && column.getType() == Constant.COLUMN_TYPE_DEFAULT) {
                    columns.remove(column);
                    break;
                }
            }
        }

        model.addAttribute("channels", channels);
        model.addAttribute("ch", channel);
        return "channel/edit";

    }

    /**
     * 更新所有频道的顺序
     */
    @RequestMapping("/sequence/update")
    @ResponseBody
    public Object updateSequence(@RequestParam(required = true) String chIds,
                                 Model model) {

        String[] idStrs = chIds.split(",");
        List ids = new ArrayList<>();
        if (idStrs.length != 0) {
            for (String idStr : idStrs) {
                try {
                    long id = Long.parseLong(idStr);
                    ids.add(id);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        channelService.updateChannelSequence(ids);

        return "";
    }

    /**
     * 更新频道状态
     */
    @RequestMapping("/status/update")
    @ResponseBody
    public Object updateChannelStatus(@RequestParam(required = true) Long channel_id) {

        String message = channelService.updateChannelStatus(channel_id);

        return message;
    }

}
