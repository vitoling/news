package com.news.controller;

import com.news.config.Constant;
import com.news.exception.BusinessError;
import com.news.exception.BusinessException;
import com.news.http.BaseResponse;
import com.news.http.ObjectResponse;
import com.news.model.ChannelEntity;
import com.news.model.ColumnEntity;
import com.news.service.ChannelService;
import com.news.service.ColumnService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/6.
 * Time : 下午8:36.
 * Description:
 */

@Controller
@RequestMapping("/column")
public class ColumnController {

    @Resource
    private ColumnService columnService;
    @Resource
    private ChannelService channelService;

    @RequestMapping("/toAppend")
    public String toAppendChannel(@RequestParam(required = true) Long channel_id,
                                  Model model) {
        model.addAttribute("channel_id", channel_id);
        return "column/append";
    }

    /**
     * 栏目保存
     */
    @RequestMapping("/save")
    @ResponseBody
    public BaseResponse saveColumn(@RequestParam(required = false, defaultValue = "-1") Long column_id,
                                   @RequestParam(required = true) String title,
                                   @RequestParam(required = true) Integer type,
                                   @RequestParam(required = true) Long channel_id) {

        ObjectResponse response = new ObjectResponse();
        ColumnEntity column;

        if (column_id == -1) {
            // 新增
            column = columnService.saveColumn(-1, title, type, channel_id, Constant.COLUMN_SAVE_OPERATION_APPEND);
        } else {
            // 更新
            column = columnService.saveColumn(column_id, title, type, channel_id, Constant.COLUMN_SAVE_OPERATION_UPDATE);
        }

        response.setCode(BaseResponse.CODE_SUCCESS);
        response.setData(column);

        return response;
    }

    /**
     * 批量删除栏目
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Object deleteColumn(@RequestParam(required = true) String deleteIds) {

        String[] ids;
        List deletedIds = new ArrayList<>();
        if (deleteIds != null && deleteIds.length() != 0) {
            ids = deleteIds.split(",");
            for (String id : ids) {
                columnService.deleteColumn(Long.valueOf(id));
                deletedIds.add(id);
            }
        } else {
            throw new BusinessException(BusinessError.ERROR_COLUMN_NOT_FOUND);
        }
        Map map = new HashMap<>();
        map.put("code", 1);
        map.put("ids", deletedIds);

        return map;

    }

    @RequestMapping("/setting")
    public String toEdit(@RequestParam(required = true) Long column_id,
                         Model model) {

        List<ChannelEntity> channels = channelService.listChannelsAndColumns();
        ColumnEntity column = columnService.getColumnAndArticlesByColumnId(column_id);
        if (column != null && channels != null && channels.size() != 0) {
            for (ChannelEntity channel : channels) {
                if (column.getChannel_id() == channel.getChannel_id()) {
                    model.addAttribute("ch", channel);
                    break;
                }
            }
        }

        model.addAttribute("column", column);
        model.addAttribute("channels", channels);
        return "column/edit";
    }

    @RequestMapping("/sequence/update")
    @ResponseBody
    public Object updateSequence(@RequestParam(required = true) String colIds,
                                 @RequestParam(required = true) Long channel_id,
                                 Model model) {


        String[] idStrs = colIds.split(",");
        List ids = new ArrayList<>();
        if (idStrs.length != 0) {
            for (String idStr : idStrs) {
                try {
                    long id = Long.parseLong(idStr);
                    ids.add(id);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        columnService.updateColumnSequenceByChannelId(channel_id, ids);

        return "";
    }

    /**
     * 更新栏目所属的频道
     */
    @RequestMapping("/channel/update")
    @ResponseBody
    public Object updateChannel(@RequestParam(required = true) Long column_id,
                                @RequestParam(required = true) Long channel_id) {


        columnService.updateChannelOfColumnByColumnIdChannelId(column_id, channel_id);

        return "";
    }


}
