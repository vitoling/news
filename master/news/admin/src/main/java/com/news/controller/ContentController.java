package com.news.controller;

import com.news.http.BaseResponse;
import com.news.http.ObjectResponse;
import com.news.service.ContentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/7.
 * Time : 下午11:37.
 * Description:
 */

@Controller
public class ContentController {

    @Resource
    private ContentService contentService;

    @RequestMapping("/content/save")
    @ResponseBody
    public BaseResponse save(@RequestParam(required = true)Long column_id){

        ObjectResponse response = new ObjectResponse();


//        ArticleEntity content = contentService.save(column_id);

        response.setCode(BaseResponse.CODE_SUCCESS);
//        response.setData();
        return response;

    }

}
