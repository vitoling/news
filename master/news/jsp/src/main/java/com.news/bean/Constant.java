package com.news.bean;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/12.
 * Time : 下午3:31.
 * Description:
 */
public class Constant {

    public static final String Login_User_Cookie_Username_Name = "newsAdminLoginUsername";
    public static final String Login_User_Cookie_Status_Name = "newsAdminLoginStatus";

    public static final Integer USER_LOGIN_STATUS_ACTIVE = 10;
    public static final Integer USER_LOGIN_STATUS_DETACHED = 20;

    public static final Integer CHANNEL_STATUS_ACTIVE = 10;
    public static final Integer CHANNEL_STATUS_DETACHED = 20;

    public static final Integer NEWES_STATUS_ACTIVE = 10;
    public static final Integer NEWES_STATUS_DETACHED = 20;

    public static final String INDEX_PAGE_URL = "/index.jsp";
    public static final String LOGIN_FALLBACK_PAGE_URL = "/login.jsp";
    public static final String CHECK_LOGIN_PAGE_URL = "/admin/checkLogin.jsp";

    public static final Integer DEFAULT_CHANNELID = -1;
}
