package com.news.dao;

import com.news.model.Admin;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by admin on 2015/10/12.
 */
public class AdminDao extends BaseDao{

    /**
     * 根据登陆名验证管理员是否存在
      */
    public Admin getAdminByUsername(String username,String password) throws Exception{
        Statement statement = null;
        ResultSet resultSet = null;

        statement = this.getConnection().createStatement();

        String sql = "SELECT * FROM admin where nick_name = '" +
                username + "' and password = '" + password + "'";
        resultSet = statement.executeQuery(sql);

        Admin admin = null;

        if (resultSet.next()){
            admin = new Admin();

            long admin_id = resultSet.getLong("admin_id");
            String name = resultSet.getString("name");
            String nick_name = resultSet.getString("nick_name");
            String psd = resultSet.getString("password");

            admin.setAdmin_id(admin_id);
            admin.setName(name);
            admin.setNick_name(nick_name);
            admin.setPassword(psd);
        }

        return admin;
     }

}
