package com.news.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/12.
 * Time : 上午8:26.
 * Description:
 */

public class BaseDao {

    private static String url = "jdbc:mysql://localhost:3306/news?useUnicode=true&characterEncoding=UTF-8";
    private static String username = "news";    // 用户名
    private static String password = "admin";    //密码
    private Connection connection = null;

    protected Connection getConnection() throws Exception {

        if (this.connection == null || this.connection.isClosed() == true) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                throw new Exception("驱动不存在");
            }

            try {
                this.connection = DriverManager.getConnection(url, username, password);
            } catch (SQLException e) {
                throw new Exception("连接数据库失败");
            }
        }

        return this.connection;
    }


}
