package com.news.dao;

import com.news.bean.Constant;
import com.news.model.Channel;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/12.
 * Time : 上午8:50.
 * Description:
 * <p>
 * 栏目Dao类
 */
public class ChannelDao extends BaseDao {

    public List listAllChannels() throws Exception {
        List<Channel> channels = new ArrayList<>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = this.getConnection().createStatement();

            String sql = "SELECT * FROM channel";
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                long channel_id = resultSet.getLong("channel_id");
                String title = resultSet.getString("title");
                int status = resultSet.getInt("status");
                long create_time = resultSet.getLong("create_time");

                Channel channel = new Channel();
                channel.setChannel_id(channel_id);
                channel.setCreate_time(create_time);
                channel.setTitle(title);
                channel.setStatus(status);
                channels.add(channel);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.getConnection().close();
            statement.close();
            resultSet.close();
        }

        return channels;
    }

    public void deleteChannelByChannelId(long id) throws Exception {
        Statement statement = null;
        try {
            statement = this.getConnection().createStatement();
            String sql = "Delete FROM channel WHERE channel_id="+id;
            statement.execute(sql);
        } catch (Exception e) {
            throw new Exception("删除栏目失败");
        }finally {
            this.getConnection().close();
            statement.close();
        }

    }

    /**
     * 根据栏目编号获取栏目信息
     */
    public Channel getChannelByChannelId(long channelId) throws Exception{
        Statement statement = null;
        ResultSet resultSet = null;
        statement = this.getConnection().createStatement();
        String sql = "select * from channel where channel_id = '" + channelId + "'";
        resultSet = statement.executeQuery(sql);
        Channel channel = null;
        if(resultSet.next()){
            long channel_id = resultSet.getLong("channel_id");
            String title = resultSet.getString("title");
            int status = resultSet.getInt("status");
            long create_time = resultSet.getLong("create_time");

            channel = new Channel();
            channel.setChannel_id(channel_id);
            channel.setTitle(title);
            channel.setStatus(status);
            channel.setCreate_time(create_time);
        }
        this.getConnection().close();
        statement.close();
        resultSet.close();

        return channel;
    }

    /**
     * 保存新增栏目或编辑栏目的信息
     */
    public void saveChannel(Channel channel) throws Exception{
        Statement statement = null;
        statement = this.getConnection().createStatement();

        String sql = null;
        //对channelId进行判断，若channelId与默认值相等，说明是新建栏目的保存
        if(channel.getChannel_id() == Long.valueOf(Constant.DEFAULT_CHANNELID)){
            sql = "insert into channel(title,create_time,status) values('"+
                    channel.getTitle() +"',"+channel.getCreate_time()+","+channel.getStatus()+")";
        }else{    //若不相等，说明是编辑栏目保存
            sql="update channel set title='" + channel.getTitle() +"',status=" +
                    channel.getStatus() + ",create_time=" + channel.getCreate_time() +
                    " where channel_id=" + channel.getChannel_id();
        }
        statement.execute(sql);

        statement.close();
        this.getConnection().close();
    }


}
