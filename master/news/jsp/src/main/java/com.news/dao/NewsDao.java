package com.news.dao;

import com.news.model.News;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class NewsDao extends BaseDao {
    public List listAllNews() throws Exception {
        List<News> newses = new ArrayList<>();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = this.getConnection().createStatement();

            String sql = "SELECT * FROM news";
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                long news_id = resultSet.getLong("news_id");
                long channel_id = resultSet.getLong("channel_id");
                String title = resultSet.getString("title");
                String content = resultSet.getString("content");
                int status = resultSet.getInt("status");
                String source = resultSet.getString("source");
                long create_time = resultSet.getLong("create_time");
                Long favorite_count = resultSet.getLong("favorite_count");

                News news = new News();
                news.setNews_id(news_id);
                news.setChannel_id(channel_id);
                news.setContent(content);
                news.setSource(source);
                news.setFavorite_count(favorite_count);
                news.setCreate_time(create_time);
                news.setTitle(title);
                news.setStatus(status);
                newses.add(news);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.getConnection().close();
            statement.close();
            resultSet.close();
        }

        return newses;
    }

    public List listChannelNews(long channel_id) throws Exception {
        List<News> newses = new ArrayList<>();
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            statement = this.getConnection().createStatement();
            String sql = "select * from news where channel_id=" + channel_id;
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                long news_id = resultSet.getLong("news_id");
                String title = resultSet.getString("title");
                String content = resultSet.getString("content");
                int status = resultSet.getInt("status");
                String source = resultSet.getString("source");
                long create_time = resultSet.getLong("create_time");
                Long favorite_count = resultSet.getLong("favorite_count");

                News news = new News();
                news.setNews_id(news_id);
                news.setChannel_id(channel_id);
                news.setContent(content);
                news.setSource(source);
                news.setFavorite_count(favorite_count);
                news.setCreate_time(create_time);
                news.setTitle(title);
                news.setStatus(status);
                newses.add(news);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.getConnection().close();
            statement.close();
            resultSet.close();
        }
        return newses;
    }

    public boolean deleteNews(long news_id) throws Exception {
        boolean success = true;
        Statement statement = null;
        try {
            statement = this.getConnection().createStatement();
            String sql = "delete from news where news_id=" + news_id;
            success = statement.execute(sql);

        } catch (Exception e) {
            throw new Exception("ɾ��ʧ�ܣ�");
        } finally {
            statement.close();
        }
        return success;
    }

    public boolean saveNews(News news) {
        String sql;

        if (news.getNews_id() == 0) {
            // 新增
            sql = "insert into news(channel_id,title,content,status,source,favorite_count,url,create_time) values (?,?,?,?,?,?,?,?)";
        } else {
            // 更新
            sql = "UPDATE news SET channel_id=?,title=?,content=?,status=?,source=?,favorite_count=?,url=? WHERE news_id=?";
        }
        PreparedStatement preparedStatement;
        try {
            preparedStatement = this.getConnection().prepareStatement(sql);
            preparedStatement.setLong(1, news.getChannel_id());
            preparedStatement.setString(2, news.getTitle());
            preparedStatement.setString(3, news.getContent());
            preparedStatement.setInt(4, news.getStatus());
            preparedStatement.setString(5, news.getSource());
            preparedStatement.setLong(6, news.getFavorite_count());
            preparedStatement.setString(7, news.getUrl());
            if (news.getNews_id() == 0) {
                preparedStatement.setLong(8, news.getCreate_time());
            } else {
                preparedStatement.setLong(8, news.getNews_id());
            }

            preparedStatement.executeUpdate();
            ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        boolean success = true;
        return success;
    }

    public News findNewsById(long news_id) {
        Statement statement = null;
        ResultSet resultSet = null;
        News news = null;
        try {
            statement = this.getConnection().createStatement();
            String sql = "select * from news where news_id=" + news_id;
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                long channel_id = resultSet.getLong("channel_id");
                String title = resultSet.getString("title");
                String content = resultSet.getString("content");
                int status = resultSet.getInt("status");
                String source = resultSet.getString("source");
                long create_time = resultSet.getLong("create_time");
                Long favorite_count = resultSet.getLong("favorite_count");

                news = new News();
                news.setNews_id(news_id);
                news.setChannel_id(channel_id);
                news.setContent(content);
                news.setSource(source);
                news.setFavorite_count(favorite_count);
                news.setCreate_time(create_time);
                news.setTitle(title);
                news.setStatus(status);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return news;
    }
}
