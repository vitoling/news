package com.news.model;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/12.
 * Time : 上午8:53.
 * Description:
 */
public class Channel {

    private Long channel_id;
    private String title;
    private Integer status;
    private Long create_time;

    public Long getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(Long channel_id) {
        this.channel_id = channel_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Long create_time) {
        this.create_time = create_time;
    }
}
