package com.news.service;

import com.news.dao.AdminDao;
import com.news.model.Admin;

/**
 * Created by admin on 2015/10/12.
 */
public class AdminService {
    private static AdminDao adminDao = new AdminDao();

    public boolean checkUserLogin(String username, String password) {
        boolean flag = false;
        try {
            Admin admin = adminDao.getAdminByUsername(username, password);
            if (null != admin) {
                flag = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return flag;
    }
}
