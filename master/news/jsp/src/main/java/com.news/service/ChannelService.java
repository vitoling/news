package com.news.service;

import com.news.dao.ChannelDao;
import com.news.dao.NewsDao;
import com.news.model.Channel;

import java.util.Date;
import java.util.List;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/12.
 * Time : 上午8:52.
 * Description:
 */
public class ChannelService {

    private static ChannelDao channelDao = new ChannelDao();
    private static NewsDao newsDao = new NewsDao();

    public List<Channel> listAllChannels() throws Exception {
        List<Channel> channels = channelDao.listAllChannels();
       return channels;
    }

    public void deleteChannelByChannelId(long id) throws Exception {

        channelDao.deleteChannelByChannelId(id);

    }

    /**
     * 根据栏目编号获取栏目信息
     */
    public Channel getChannelByChannel_id(long channelId){
        Channel channel = null;
        try {
            channel = channelDao.getChannelByChannelId(channelId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return channel;
    }

    /**
     * 保存新增栏目或编辑栏目的信息
     */
    public void saveChannel(long channelId,String title,int status) {
        long now = new Date().getTime();
        Channel channel = new Channel();
        channel.setChannel_id(channelId);
        channel.setTitle(title);
        channel.setStatus(status);
        channel.setCreate_time(now);
        try {
            channelDao.saveChannel(channel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
