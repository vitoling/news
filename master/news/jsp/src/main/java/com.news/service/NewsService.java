package com.news.service;

import com.news.dao.NewsDao;
import com.news.model.News;

import java.util.List;

/**
 * Created by maybe on 2015-10-12.
 */
public class NewsService {
    private static NewsDao newsDao = new NewsDao();

    public List<News> listAllNews() throws Exception {
        List<News> newses = newsDao.listAllNews();
        return newses;
    }

    public List<News> listChannelNews(long channel_id) throws Exception {
        List<News> newses = newsDao.listChannelNews(channel_id);
        return newses;
    }

    public boolean deleteNews(long news_id) {
        boolean flag = true;
        try {
            flag = newsDao.deleteNews(news_id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public News findNewsById(long news_id) throws Exception {
        News news = newsDao.findNewsById(news_id);
        if (news == null) {
            throw new Exception("新闻不存在");
        }
        return news;
    }

    public boolean saveNews(News news) {

        newsDao.saveNews(news);
        return true;
    }
}
