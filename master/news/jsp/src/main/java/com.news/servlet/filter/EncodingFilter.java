package com.news.servlet.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/12.
 * Time : 下午1:37.
 * Description:
 * <p>
 * 编码过滤器
 */

public class EncodingFilter implements Filter {
    public void destroy() {

    }

    public void doFilter(ServletRequest req, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        request.setCharacterEncoding("utf-8");

        chain.doFilter(req, response);//交给下一个过滤器
    }

    public void init(FilterConfig config) throws ServletException {

    }
}