<%@ page import="com.news.bean.Constant" %>
<%--
  Created by IntelliJ IDEA.
  User: vitoling
  Date: 15/10/12
  Time: 上午11:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="adminService" class="com.news.service.AdminService"></jsp:useBean>

<html>
<head>
    <title></title>
</head>
<body>
<%

    String username = request.getParameter("username");
    String password = request.getParameter("password");

    boolean result = adminService.checkUserLogin(username, password);
    if (result) {
        //创建cookie对象
        Cookie usernameCookie = new Cookie(Constant.Login_User_Cookie_Username_Name, username);
        Cookie loginStatusCookie = new Cookie(Constant.Login_User_Cookie_Status_Name, String.valueOf(Constant.USER_LOGIN_STATUS_ACTIVE));
        //设置cookie为 60 * 30 s
        usernameCookie.setMaxAge(60 * 30);
        loginStatusCookie.setMaxAge(60 * 30);
        usernameCookie.setPath("/");
        loginStatusCookie.setPath("/");
        //添加cookie，让其生效
        response.addCookie(usernameCookie);
        response.addCookie(loginStatusCookie);
        response.sendRedirect(Constant.INDEX_PAGE_URL);
    } else {
        response.sendRedirect(Constant.LOGIN_FALLBACK_PAGE_URL);
    }

%>

</body>
</html>
