<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en-us"
      xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title> 新闻管理系统后台管理 </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Basic Styles -->
  <link rel="stylesheet" type="text/css" media="screen" href="../static/css/bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" media="screen" href="../static/css/font-awesome.min.css"/>

  <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
  <link rel="stylesheet" type="text/css" media="screen" href="../static/css/smartadmin-production-plugins.min.css"/>
  <link rel="stylesheet" type="text/css" media="screen" href="../static/css/smartadmin-production.min.css"/>
  <link rel="stylesheet" type="text/css" media="screen" href="../static/css/smartadmin-skins.min.css">

  <!-- FAVICONS -->
  <link rel="shortcut icon" href="../static/img/favicon/favicon.ico" type="image/x-icon"/>
  <link rel="icon" href="../static/img/favicon/favicon.ico" type="image/x-icon"/>

</head>

<body class="">

<!-- HEADER -->
<header id="header">
  <div id="logo-group">

    <!-- PLACE YOUR LOGO HERE -->
    <span id="logo"><a href="/index.jsp">新闻管理后台</a></span>
    <!-- END LOGO PLACEHOLDER -->

  </div>

  <!-- pulled right: nav area -->
  <div class="pull-right">

    <!-- collapse menu button -->
    <div id="hide-menu" class="btn-header pull-right">
            <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i
                    class="fa fa-reorder"></i></a> </span>
    </div>
    <!-- end collapse menu -->

    <!-- logout button -->
    <div id="logout" class="btn-header transparent pull-right">
            <span> <a href="/login.jsp" title="登出" data-action="userLogout" data-logout-msg="您确定要退出登录吗?"><i
                    class="fa fa-sign-out"></i></a> </span>
    </div>
    <!-- end logout button -->

    <!-- fullscreen button -->
    <div id="fullscreen" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="全屏"><i
                    class="fa fa-arrows-alt"></i></a> </span>
    </div>
    <!-- end fullscreen button -->
    <!-- home button -->
    <div id="home" class="btn-header transparent pull-right">
      <span> <a href="/index.jsp" title="回到首页"><i class="fa fa-home"></i></a> </span>
    </div>
    <!-- end home button -->

  </div>
  <!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->

<div id="" role="main">

  <!-- RIBBON -->
  <div id="ribbon">

    <!-- breadcrumb -->
    <ol class="breadcrumb">
      <i class="fa fa-arrow-right"></i>
      <li class="fa fa-lg" >增加栏目</li>
    </ol>

  </div>
  <!-- END RIBBON -->

  <div id="content">
    <!-- widget grid -->
    <section id="widget-grid" class="">

      <!-- START ROW -->
      <div class="row">

        <!-- NEW COL START -->
        <article class="col-lg-6 col-lg-offset-3">

          <!-- Widget ID (each widget will need unique ID)-->
          <div class="jarviswidget jarviswidget-color-green" id="wid-id-0" data-widget-editbutton="false"
               data-widget-custombutton="false">

            <header>
              <span class="widget-icon"> <i class="fa fa-edit"></i> </span>

              <h2>增加栏目</h2>

            </header>

            <!-- widget div-->
            <div>

              <!-- widget content -->
              <div class="widget-body no-padding">

                <form id="channelAppendForm" class="smart-form" action="save.jsp" method="post">

                  <header>
                    请填写标题、选择状态：
                  </header>

                  <fieldset>
                    <div class="row">
                      <section class="col col-4">
                        <label class="input">
                          栏目号<input type="text" disabled="disabled" class="form-control" placeholder="自动分配"/>
                        </label>
                      </section>

                      <section class="col col-4">
                        <label class="input">
                          栏目标题<input type="text" name="title" required="required"
                                     class="form-control" maxlength="10" placeholder="至多10个字"/>
                        </label>
                      </section>
                    </div>

                    <div class="row">
                      <section class="col col-4">
                        <label class="input">
                          创建时间<input type="text" disabled="disabled" class="form-control" placeholder="自动分配"/>
                        </label>
                      </section>

                      <section class="col col-3">
                        <br>
                        <label class="toggle">
                          <input type="checkbox" name="status" id="status-toggle">
                          <i data-swchon-text="上线" data-swchoff-text="下线"></i>状态</label>

                      </section>
                    </div>
                  </fieldset>

                  <footer>
                    <button type="submit" class="btn btn-primary">
                      <i class="fa fa-share-square"></i>&nbsp;提交
                    </button>
                    <button type="button" class="btn btn-default" onclick="window.history.back();">
                      <i class="fa fa-backward"></i>&nbsp;返回
                    </button>
                  </footer>
                </form>

              </div>
              <!-- end widget content -->

            </div>
            <!-- end widget div -->

          </div>
          <!-- end widget -->

        </article>
        <!-- END COL -->

      </div>
    </section>

  </div>

</div>


<!-- PAGE FOOTER -->
<div class="page-footer">
  <div class="row">
    <div class="col-xs-12 col-sm-6">
      <span class="txt-color-white">新闻管理系统</span>
    </div>

  </div>
</div>
<!-- END PAGE FOOTER -->

<script src="../static/js/libs/jquery-2.1.1.min.js"></script>

<script src="../static/js/libs/jquery-ui-1.10.3.min.js"></script>

<!-- IMPORTANT: APP CONFIG -->
<script src="../static/js/app.config.js"></script>

<!-- BOOTSTRAP JS -->
<script src="../static/js/bootstrap/bootstrap.min.js"></script>

<!-- JARVIS WIDGETS -->
<script src="../static/js/libs/jarvis.widget.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="../static/js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="../static/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="../static/js/plugin/select2/select2.min.js"></script>

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<!-- MAIN APP JS FILE -->
<script src="../static/js/app.min.js"></script>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /*<![CDATA[*/

  // DO NOT REMOVE : GLOBAL FUNCTIONS!
  $(document).ready(function () {

    pageSetUp();

  });

  /*]]>*/

</script>

</body>
</html>
