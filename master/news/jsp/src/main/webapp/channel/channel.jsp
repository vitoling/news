<%@ page import="com.news.bean.Constant" %>
<%@ page import="com.news.model.Channel" %>
<%@ page import="com.news.service.ChannelService" %>
<%@ page import="com.news.util.DateUtil" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: vitoling
  Date: 15/10/12
  Time: 上午8:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="channelService" class="com.news.service.ChannelService"/>
<!DOCTYPE html>
<html lang="en-us"
      xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="utf-8">
  <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

  <title> 新闻管理系统后台管理 </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

  <!-- Basic Styles -->
  <link rel="stylesheet" type="text/css" media="screen" href="../static/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" media="screen" href="../static/css/font-awesome.min.css">

  <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
  <link rel="stylesheet" type="text/css" media="screen" href="../static/css/smartadmin-production-plugins.min.css">
  <link rel="stylesheet" type="text/css" media="screen" href="../static/css/smartadmin-production.min.css">
  <link rel="stylesheet" type="text/css" media="screen" href="../static/css/smartadmin-skins.min.css">



</head>

<body class="">

<!-- HEADER -->
<header id="header">
  <div id="logo-group">

    <!-- PLACE YOUR LOGO HERE -->
    <span id="logo"><a href="../index.jsp">新闻管理后台</a></span>
    <!-- END LOGO PLACEHOLDER -->

  </div>

  <!-- pulled right: nav area -->
  <div class="pull-right">

    <!-- collapse menu button -->
    <div id="hide-menu" class="btn-header pull-right">
            <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i
                    class="fa fa-reorder"></i></a> </span>
    </div>
    <!-- end collapse menu -->

    <!-- logout button -->
    <div id="logout" class="btn-header transparent pull-right">
            <span> <a href="../login.jsp" title="登出" data-action="userLogout" data-logout-msg="您确定要退出登录吗?"><i
                    class="fa fa-sign-out"></i></a> </span>
    </div>

    <!-- end logout button -->

    <!-- fullscreen button -->
    <div id="fullscreen" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="全屏"><i
                    class="fa fa-arrows-alt"></i></a> </span>
    </div>
    <!-- end fullscreen button -->
    <!-- home button -->
    <div id="home" class="btn-header transparent pull-right">
      <span> <a href="../index.jsp" title="回到首页"><i class="fa fa-home"></i></a> </span>
    </div>
    <!-- end home button -->

  </div>
  <!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->

<!-- Left panel : Navigation area -->
<aside id="left-panel">
  <nav>
    <ul>
      <li>
        <a href="../index.jsp">
          <i class="fa fa-lg fa-book"></i>
          <span class="menu-item-parent fa-lg">首页</span>
        </a>
      </li>
      <li class="active">
        <a href="../channel/channel.jsp">
          <i class="fa fa-lg fa-book"></i>
          <span class="menu-item-parent fa-lg">栏目管理</span>
        </a>
      </li>
      <li>
        <a href="javascript:">
          <i class="fa fa-lg fa-book"></i>
          <span class="menu-item-parent fa-lg">新闻管理</span>
        </a>
        <ul>
          <%
            List<Channel> channels=null;
            try {
              channels = channelService.listAllChannels();
            } catch (Exception e) {
              e.printStackTrace();
            }
          %>
          <%
            for (Channel channel : channels) {
          %>
          <li class="channelLi">
            <a href="../news/list.jsp?channel_id=<%=channel.getChannel_id()%>">
              <i class="fa fa-lg fa-edit"></i>
              <span class="menu-item-parent fa-lg"><%=channel.getTitle()%></span>
            </a>
          </li>
          <%
            }
          %>
        </ul>
      </li>
    </ul>
  </nav>
</aside>
<!-- END NAVIGATION -->
<!-- MAIN PANEL -->
<div id="main" role="main">

  <!-- RIBBON -->
  <div id="ribbon">
    <!-- breadcrumb -->
    <ol class="breadcrumb">
      <i class="fa fa-arrow-right"></i>
      <li class="fa fa-lg">欢迎您</li>
    </ol>
    <!-- end breadcrumb -->
  </div>

  <!-- MAIN CONTENT -->
  <div id="content">


    <!-- widget grid -->
    <section id="widget-grid" class="">

      <!-- row -->

      <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-offset-2 col-lg-8">

          <!-- Widget ID (each widget will need unique ID)-->
          <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">

            <header>
              <span class="widget-icon"> <i class="fa fa-map-marker"></i> </span>
            </header>

            <!-- widget div-->
            <div>

              <!-- widget content -->
              <div class="widget-body no-padding">

                <form class="smart-form" method="post" action="delete.jsp">
                  <header>
                    栏目列表
                  </header>

                  <fieldset>
                        <table class="table table-hover">
                          <thead>
                          <tr>
                            <th>栏目号</th>
                            <th>栏目标题</th>
                            <th>创建时间</th>
                            <th>状态</th>
                            <th>

                            </th>
                          </tr>
                          </thead>
                          <tbody>
                          <%
                            List<Channel> channels2=null;
                            ChannelService channelService2 =new ChannelService();
                            try {
                              channels2 = channelService2.listAllChannels();
                            } catch (Exception e) {
                              e.printStackTrace();
                            }
                          %>
                          <%
                            if (channels2 != null && channels2.size() != 0){
                              for (Channel channel : channels2){%>
                          <tr>
                            <td><%=channel.getChannel_id()%></td>
                            <td><%=channel.getTitle()%></td>
                            <td><%=DateUtil.longToString(channel.getCreate_time(), DateUtil.DATE_FORMAT_YMDHMS)%></td>
                            <%if(channel.getStatus()== Constant.CHANNEL_STATUS_ACTIVE){%>
                            <td><p class="text-success"><%="已上线"%></p></td>
                            <%
                            }else{
                            %>
                            <td><p class="text-warning"><%="已下线"%></p></td>
                            <%
                              }
                            %>
                            <td>
                              <button type="button" class="btn btn-warning"
                                      onclick="window.location.href='update.jsp?channelId=<%=channel.getChannel_id()%>'">编辑</button>
                              <button type="button" class="btn btn-danger"
                                      onclick="deleteChannel(<%=channel.getChannel_id()%>)">删除</button>
                            </td>
                          </tr>
                          <% }
                          }
                          %>
                          </tbody>
                        </table>
                        <script type="text/javascript">
                          function deleteChannel(channel_id){
                            if(confirm('确定删除数据？？')){
                              window.location.href='delete.jsp?channel_id='+channel_id;
                            }
                          }
                        </script>

                  </fieldset>

                  <footer>
                    <button type="button" id="refresh_info_btn" onclick="location.reload()"
                            class="btn btn-success">
                      <i class="fa fa-refresh"></i>
                      刷新
                    </button>
                    <button type="button" class="btn btn-primary" onclick="location.href='append.jsp'"><i class="fa fa-plus"></i>增加</button>
                    <button type="button" onclick="location.href='../index.jsp';" class="btn btn-danger"><i class="fa fa-backward"></i>返回</button>
                  </footer>

                </form>

              </div>
              <!-- end widget content -->

            </div>
            <!-- end widget div -->


          </div>
          <!-- end widget -->


        </article>
        <!-- WIDGET END -->

      </div>

      <!-- end row -->

    </section>
    <!-- end widget grid -->

  </div>
  <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->


<!-- PAGE FOOTER -->
<div class="page-footer">
  <div class="row">
    <div class="col-xs-12 col-sm-6">
      <span class="txt-color-white">新闻管理系统</span>
    </div>

  </div>
</div>
<!-- END PAGE FOOTER -->

<!--================================================== -->

<script src="../static/js/libs/jquery-2.1.1.min.js"></script>

<script src="../static/js/libs/jquery-ui-1.10.3.min.js"></script>

<!-- IMPORTANT: APP CONFIG -->
<script src="../static/js/app.config.js"></script>

<!-- BOOTSTRAP JS -->
<script src="../static/js/bootstrap/bootstrap.min.js"></script>

<!-- JARVIS WIDGETS -->
<script src="../static/js/libs/jarvis.widget.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="../static/js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="../static/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="../static/js/plugin/select2/select2.min.js"></script>

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<!-- MAIN APP JS FILE -->
<script src="../static/js/app.min.js"></script>

<script>
  /*<![CDATA[*/
  // DO NOT REMOVE : GLOBAL FUNCTIONS!
  $(function () {

    pageSetUp();

  })
  /*]]>*/
</script>

</body>

</html>



