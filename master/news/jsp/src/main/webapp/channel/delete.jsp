<%--
  Created by IntelliJ IDEA.
  User: 蔡超
  Date: 2015/10/12
  Time: 14:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="channelService" class="com.news.service.ChannelService"></jsp:useBean>

<%
    try {
        String idStr = request.getParameter("channel_id");
        Long channel_id = null;
        if (idStr != null) {
            channel_id = Long.valueOf(idStr);
        }
        channelService.deleteChannelByChannelId(channel_id);
    } catch (Exception e) {
        e.printStackTrace();
    }
%>
<script>
    location.href = "channel.jsp";
</script>