<%@ page import="com.news.bean.Constant" %>

<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 2015/10/13
  Time: 11:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="channel" class="com.news.service.ChannelService"></jsp:useBean>

<html>
<head>
    <title></title>
</head>
<body>

<%
    String title = request.getParameter("title");
    String statusStr = request.getParameter("status");
    int status = (statusStr != null && statusStr.equals("on")?Constant.CHANNEL_STATUS_ACTIVE:Constant.CHANNEL_STATUS_DETACHED);
    String tempChannelId = request.getParameter("channel_id");   //获取新增页面或编辑页面传来的channelId

    long channelId = 0;
    if(tempChannelId == null){   //对获取的channelId进行判断，若为空，说明是新增保存，给它设置一个默认值
        channelId = Long.valueOf(Constant.DEFAULT_CHANNELID);
    }else {    //若不为空，说明是修改保存，将它转换成规范的格式
        channelId = Long.parseLong(tempChannelId);
    }
    channel.saveChannel(channelId,title, status);
    response.sendRedirect("channel.jsp");
%>

</body>
</html>
