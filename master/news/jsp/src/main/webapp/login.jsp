<%@ page import="com.news.bean.Constant" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content=""/>
    <meta name="author" content=""/>

    <title>新闻后台管理</title>

    <!-- Bootstrap core CSS -->
    <link href="static/css/bootstrap.min.css" rel="stylesheet"/>

    <!-- Custom styles for this template -->
    <link href="static/css/signin.css" rel="stylesheet"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<%

    // 设置进入登录页面就清除客户端的登录cookie
    Cookie[] cookies = request.getCookies();
    if (null != cookies && cookies.length != 0) {
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(Constant.Login_User_Cookie_Status_Name)) {
                cookie.setValue(String.valueOf(Constant.USER_LOGIN_STATUS_DETACHED));
                response.addCookie(cookie);

            } else {
                if (cookie.getName().equals(Constant.Login_User_Cookie_Username_Name)) {
                    cookie.setValue(null);
                    response.addCookie(cookie);
                }
            }
        }
    }


%>
<div class="container">

    <form class="form-signin" action="<%=Constant.CHECK_LOGIN_PAGE_URL%>" method="post">
        <h2 class="form-signin-heading">请登录</h2>
        <label for="inputUserName" class="sr-only">用户名</label>
        <input type="text" id="inputUserName" name="username" class="form-control" placeholder="请输入用户名"
               required="required" autofocus="autofocus"/>
        <br/>
        <label for="inputPassword" class="sr-only">输入密码</label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="请输入密码"
               required="required"/>

        <button class="btn btn-lg btn-primary btn-block" type="submit">登录</button>
    </form>

</div>
<!-- /container -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="//cdn.bootcss.com/jquery/1.11.3/jquery.min.js"></script>-->
<!--<script src="//cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>-->
<script src="static/js/libs/jquery-2.1.1.min.js"></script>
<script src="static/js/bootstrap/bootstrap.min.js"></script>

</body>
</html>

