<%@ page import="com.news.model.Channel" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: maybe
  Date: 2015-10-14
  Time: 10:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="channelService" class="com.news.service.ChannelService"></jsp:useBean>
<%
    List<Channel> channels = channelService.listAllChannels();
    String channel_id_str = request.getParameter("channel_id");
%>
<html>
<head>
    <title>新闻新增界面</title>
</head>
<body onload="loadtxt()">
<form action="save.jsp">
    <table align="right">
        <tr align="right">
            <td align="right"><a href="list.jsp?channel_id=<%=channel_id_str%>">返回</a></td>
        </tr>
    </table>
    <br><br><br><br><br>
    <table align="center">
        <tr align="center">
            <td>标题<input type="text" name="title"></td>
        </tr>
        <tr align="center">
            <td>作者<input type="text" name="source"></td>
        </tr>
        <tr align="center">
            <td>栏目
                <select name="channel_id">
                    <%
                        for (Channel channel : channels) {
                    %>
                    <option value="<%=channel.getChannel_id()%>"><%=channel.getTitle()%>
                    </option>
                    <%
                        }
                    %>
                </select>
            </td>
        </tr>
        <tr>
            <td>正文<textarea id="txt" type="text" name="content"></textarea></td>
        </tr>
        <tr>
            <td align="center">
                <input type="hidden" name="favorite_count" value="0">
                <input type="hidden" name="status" value="off">
                <input type="submit" value="提交">
                <input type="reset" value="重置">
            </td>
        </tr>
    </table>
</form>
<script src="../static/js/plugin/ckeditor/ckeditor.js"></script>
<script>
    function loadtxt() {
        CKEDITOR.replace('txt', {height: '200px'});
    }
</script>
</body>
</html>
