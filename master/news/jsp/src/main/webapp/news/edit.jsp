<%@ page import="com.news.model.Channel" %>
<%@ page import="com.news.model.News" %>
<%@ page import="com.news.util.DateUtil" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: vitoling
  Date: 15/10/15
  Time: 上午8:34
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="newsService" class="com.news.service.NewsService"></jsp:useBean>
<jsp:useBean id="channelService" class="com.news.service.ChannelService"></jsp:useBean>
<!DOCTYPE html>
<html lang="en-us"
      xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

    <title> 新闻后台管理 </title>
    <meta name="description" content=""/>
    <meta name="author" content=""/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="../static/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="../static/css/font-awesome.min.css"/>

    <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
    <link rel="stylesheet" type="text/css" media="screen" href="../static/css/smartadmin-production-plugins.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="../static/css/smartadmin-production.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="../static/css/smartadmin-skins.min.css">

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="../static/img/favicon/favicon.ico" type="image/x-icon"/>
    <link rel="icon" href="../static/img/favicon/favicon.ico" type="image/x-icon"/>

</head>

<body class="">

<%

    String newsIdStr = request.getParameter("news_id");
    long news_id = Long.parseLong(newsIdStr);
    News news = newsService.findNewsById(news_id);
    List<Channel> channels = channelService.listAllChannels();

%>

<!-- HEADER -->
<header id="header">
    <div id="logo-group">

        <!-- PLACE YOUR LOGO HERE -->
        <span id="logo"><a href="/index.jsp">新闻管理后台</a></span>
        <!-- END LOGO PLACEHOLDER -->

    </div>

    <!-- pulled right: nav area -->
    <div class="pull-right">

        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
            <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i
                    class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->

        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
            <span> <a href="/login.jsp" title="登出" data-action="userLogout" data-logout-msg="您确定要退出登录吗?"><i
                    class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->

        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="全屏"><i
                    class="fa fa-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->
        <!-- home button -->
        <div id="home" class="btn-header transparent pull-right">
            <span> <a href="/index.jsp" title="回到首页"><i class="fa fa-home"></i></a> </span>
        </div>
        <!-- end home button -->

    </div>
    <!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->

<!-- MAIN PANEL -->
<div id="" role="main">

    <!-- RIBBON -->
    <div id="ribbon">

        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <i class="fa fa-arrow-right"></i>
            <li class="fa fa-lg">编辑新闻 - <%=news.getTitle()%>
            </li>
        </ol>

    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <!-- row -->
            <div class="row">

                <article class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-green" id="wid-id-0" data-widget-colorbutton="false"
                         data-widget-editbutton="false" data-widget-deletebutton="false"
                         data-widget-custombutton="false">

                        <header>
                            <span class="widget-icon"> <i class="fa fa-info-circle"></i></span>
                            <h2>编辑新闻</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                <form class="smart-form " id="editForm" action="save.jsp" method="post">
                                    <header>
                                        新闻详情
                                    </header>

                                    <fieldset>
                                        <input hidden="hidden" value="<%=news.getNews_id()%>" name="news_id"/>
                                        <div class="row">
                                            <section class="col col-6">
                                                <section class="col-xs-10">
                                                    <label class="label font-lg">标题：</label>
                                                    <input class="font-md col-xs-12" name="title"
                                                           value="<%=news.getTitle()%>"/>
                                                </section>
                                                <section class="col-xs-10">
                                                    <label class="label font-lg">来源：</label>
                                                    <input class="font-md col-xs-12" value="<%=news.getSource()%>"
                                                           name="source"/>
                                                </section>

                                                <section class="col-xs-10">
                                                    <label class="label">所属栏目：</label>
                                                    <label class="select">
                                                        <select class="input-lg " id="channel"
                                                                data-channel-id="<%=news.getChannel_id()%>"
                                                                name="channel_id">
                                                            <%
                                                                for (Channel channel : channels) {
                                                            %>
                                                            <option value="<%=channel.getChannel_id()%>"><%=channel.getTitle()%>
                                                            </option>
                                                            <%
                                                                }
                                                            %>
                                                        </select><i></i>
                                                    </label>
                                                </section>

                                            </section>
                                            <section class="col col-6">
                                                <section class="col-lg-12">
                                                    <label class="label font-lg">收藏数：</label>
                                                    <input class="font-md col-xs-6"
                                                           value="<%=news.getFavorite_count()%>" name="favorite_count"/>
                                                </section>
                                                <section class="col-lg-12">
                                                    <label class="label font-lg">创建时间：</label>
                                                    <input class="font-md col-xs-6"
                                                           value="<%=DateUtil.longToString(news.getCreate_time(),DateUtil.DATE_FORMAT_YMDHMS)%>"/>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="toggle">
                                                        <input type="checkbox" name="status" id="status-toggle"
                                                               data-status="<%=news.getStatus()%>">
                                                        <i data-swchon-text="上线" data-swchoff-text="下线"></i>上线状态</label>
                                                    <div class="note">
                                                        <strong>提示:</strong>点击即可修改上线状态，需保存
                                                    </div>
                                                </section>

                                            </section>

                                        </div>
                                    </fieldset>
                                    <header>
                                        编辑内容
                                    </header>

                                    <fieldset>

                                        <div class="row">
                                            <textarea id="editContentBox"
                                                      name="content"><%=news.getContent()%></textarea>
                                        </div>
                                    </fieldset>
                                    <footer>
                                        <!-- 保存按钮 -->
                                        <button type="submit" id="save_info_btn" class="btn btn-primary">
                                            <i class="fa fa-save"></i>
                                            保存
                                        </button>
                                        <!-- 返回按钮 -->
                                        <button type="button" onclick="javascript:history.back();"
                                                class="back_btn btn btn-danger">
                                            <i class="fa fa-backward"></i>
                                            返回
                                        </button>
                                    </footer>

                                </form>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                    <!-- end widget -->

                </article>

            </div>


        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->

<!-- PAGE FOOTER -->
<div class="page-footer">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <span class="txt-color-white">新闻管理系统</span>
        </div>

    </div>
</div>
<!-- END PAGE FOOTER -->


<!--================================================== -->

<script src="../static/js/libs/jquery-2.1.1.min.js"></script>

<script src="../static/js/libs/jquery-ui-1.10.3.min.js"></script>

<!-- IMPORTANT: APP CONFIG -->
<script src="../static/js/app.config.js"></script>

<!-- BOOTSTRAP JS -->
<script src="../static/js/bootstrap/bootstrap.min.js"></script>

<!-- JARVIS WIDGETS -->
<script src="../static/js/libs/jarvis.widget.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="../static/js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="../static/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="../static/js/plugin/select2/select2.min.js"></script>

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<!-- MAIN APP JS FILE -->
<script src="../static/js/app.min.js"></script>
<script src="../static/js/plugin/jquery-nestable/jquery.nestable.min.js"></script>

<script src="../static/js/plugin/ckeditor/ckeditor.js"></script>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
    /*<![CDATA[*/

    var column_id;

    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    $(document).ready(function () {

        pageSetUp();

        var channel_id = $('#channel').data('channel-id');
        $('#channel').val(channel_id);
        var status = $("#status-toggle").data("status");
        if (status == 10) {
            $("#status-toggle").attr("checked", "checked");
        }
        CKEDITOR.replace('editContentBox', {height: '700px'});

    });


    /*]]>*/

</script>


</body>

</html>
