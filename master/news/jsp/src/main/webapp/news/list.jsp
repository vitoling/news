<%@ page import="com.news.model.Channel" %>
<%@ page import="com.news.model.News" %>
<%@ page import="com.news.util.DateUtil" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: vitoling
  Date: 15/10/12
  Time: 上午8:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="channelService" class="com.news.service.ChannelService"/>
<jsp:useBean id="newsService" class="com.news.service.NewsService"></jsp:useBean>
<!DOCTYPE html>
<html lang="en-us"
      xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

    <title> 新闻管理系统后台管理 </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="../static/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../static/css/font-awesome.min.css">

    <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
    <link rel="stylesheet" type="text/css" media="screen" href="../static/css/smartadmin-production-plugins.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../static/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../static/css/smartadmin-skins.min.css">


    <style type="text/css">
        body, html, #map {
            width: 100%;
            height: 100%;
            overflow: hidden;
            margin: 0;
        }
    </style>


</head>

<body class="">

<!-- HEADER -->
<header id="header">
    <div id="logo-group">

        <!-- PLACE YOUR LOGO HERE -->
        <span id="logo"><a href="../index.jsp">新闻管理后台</a></span>
        <!-- END LOGO PLACEHOLDER -->

    </div>

    <!-- pulled right: nav area -->
    <div class="pull-right">

        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
            <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i
                    class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->

        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
            <span> <a href="../login.jsp" title="登出" data-action="userLogout" data-logout-msg="您确定要退出登录吗?"><i
                    class="fa fa-sign-out"></i></a> </span>
        </div>

        <!-- end logout button -->

        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="全屏"><i
                    class="fa fa-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->
        <!-- home button -->
        <div id="home" class="btn-header transparent pull-right">
            <span> <a href="../index.jsp" title="回到首页"><i class="fa fa-home"></i></a> </span>
        </div>
        <!-- end home button -->

    </div>
    <!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->

<!-- Left panel : Navigation area -->
<aside id="left-panel">
    <nav>
        <ul>
            <li>
                <a href="../index.jsp">
                    <i class="fa fa-lg fa-book"></i>
                    <span class="menu-item-parent fa-lg">首页</span>
                </a>
            </li>
            <li>
                <a href="../channel/channel.jsp">
                    <i class="fa fa-lg fa-book"></i>
                    <span class="menu-item-parent fa-lg">栏目管理</span>
                </a>
            </li>
            <li class="active">
                <a href="javascript:">
                    <i class="fa fa-lg fa-book"></i>
                    <span class="menu-item-parent fa-lg">新闻管理</span>
                </a>
                <ul>
                    <%
                        List<Channel> channels = null;
                        try {
                            channels = channelService.listAllChannels();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    %>
                    <%
                        for (Channel channel : channels) {
                    %>
                    <li class="channelLi">
                        <a href="../news/list.jsp?channel_id=<%=channel.getChannel_id()%>">
                            <i class="fa fa-lg fa-edit"></i>
                            <span class="menu-item-parent fa-lg"><%=channel.getTitle()%></span>
                        </a>
                    </li>
                    <%
                        }
                    %>
                </ul>
            </li>
        </ul>
    </nav>
</aside>
<!-- END NAVIGATION -->
<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <div id="ribbon">
        <!-- breadcrumb -->
        <ol class="breadcrumb">
            <i class="fa fa-arrow-right"></i>
            <li class="fa fa-lg">欢迎您</li>
        </ol>
        <!-- end breadcrumb -->
    </div>

    <!-- MAIN CONTENT -->
    <div id="content">


        <!-- END RIBBON -->
        <!-- widget grid -->
        <section id="widget-grid" class="">

            <!-- row -->

            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-xs-12 col-sm-12 col-lg-offset-2 col-lg-8">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-0" data-widget-editbutton="false">

                        <header>
                            <span class="widget-icon"> <i class="fa fa-map-marker"></i> </span>
                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                <form class="smart-form" method="post" action="delete.jsp">
                                    <header>
                                        新闻列表
                                    </header>

                                    <fieldset>
                                        <%
                                            String channelIdStr = request.getParameter("channel_id");
                                            List<News> newses = null;
                                            long channel_id = Long.parseLong(channelIdStr);
                                            try {
                                                if (channelIdStr.isEmpty()) {
                                                    newses = newsService.listAllNews();
                                                } else {
                                                    newses = newsService.listChannelNews(channel_id);
                                                }
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }
                                        %>

                                        <table width="100%">
                                            <tr>
                                                <td align="right">
                                                    作者： <input type="text" name="">
                                                    标题： <input type="text" name="">
                                                    <input type="button" name="" value="搜索">
                                                    <input type="button" name="" value="新增" onclick="add_news()">
                                                    <input type="button" name="" value="删除" onclick="delete_news()">
                                                    <input type="button" name="" value="返回" onclick="back_to_index()">
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" border="1" align="center">
                                            <tr align="center">
                                                <td width="40" align="center"><input type="checkbox" name="checkAll"
                                                                                     onclick="checkAll_news()"></td>
                                                <td width="40">序号</td>
                                                <td>标题</td>
                                                <td>略文</td>
                                                <td>作者</td>
                                                <td>栏目</td>
                                                <td>发表时间</td>
                                                <td>收藏数</td>
                                                <td>状态</td>
                                                <td width="80">操作</td>
                                            </tr>

                                            <%
                                                int index = 0;
                                                for (News news : newses) {
                                                    String news_content = news.getContent();
                                                    if (news_content.length() >= 20) {
                                                        news_content = news_content.substring(0, 20);
                                                    }
                                            %>
                                            <tr align="center">

                                                <td><input type="checkbox" name="checked_news"
                                                           value="<%=news.getNews_id()%>"></td>
                                                <td><%=++index%>
                                                </td>
                                                <td><%=news.getTitle()%>
                                                </td>
                                                <td><%=news_content%>
                                                </td>
                                                <td><%=news.getSource()%>
                                                </td>
                                                <td><%=channelService.getChannelByChannel_id(news.getChannel_id()).getTitle()%>
                                                </td>
                                                <td><%=DateUtil.longToString(news.getCreate_time(), DateUtil.DATE_FORMAT_YMDHMS)%>
                                                </td>
                                                <td><%=news.getFavorite_count()%>
                                                </td>
                                                <td><%=news.getStatus().equals(10) ? "已上线" : "未上线"%>
                                                </td>
                                                <td><a href="edit.jsp?news_id=<%=news.getNews_id()%>">
                                                    编辑</a><a
                                                        href="delete.jsp?news_id=<%=news.getNews_id()%>&channel_id=<%=news.getChannel_id()%>">

                                                    删除</a></td>
                                            </tr>
                                            <%
                                                }
                                            %>

                                        </table>

                                        <script src="../static/js/libs/jquery-2.1.1.min.js"></script>
                                        <script>
                                            function back_to_index() {
                                                window.location.href = "../index.jsp";
                                            }
                                            function add_news() {
                                                window.location.href = "append.jsp?channel_id=<%=channel_id%>";
                                            }
                                            function delete_news() {
                                                var news_ids = "";
                                                var checked_news = document.getElementsByName("checked_news");
                                                for (i = 0; i < checked_news.length; i++) {
                                                    if (checked_news.item(i).checked) {
                                                        news_ids += "," + checked_news.item(i).value;
                                                    }
                                                }
                                                if (news_ids == "") {
                                                    alert("请选择要删除的数据！");
                                                } else {
                                                    if (confirm("确认删除选中记录？")) {
                                                        news_ids = news_ids.substring(1);
                                                        window.location.href = "delete.jsp?channel_id=<%=channel_id%>&news_ids=" + news_ids;
                                                    }
                                                }
                                                //alert(news_ids);
                                            }
                                            function checkAll_news() {

                                                var checked_newses = document.getElementsByName("checked_news");
                                                var checkAll_news = document.getElementsByName("checkAll");

                                                for (var i = 0; i < checked_newses.length; i++) {
                                                    checked_newses.item(i).checked = checkAll_news.item(0).checked;
                                                }
                                            }
                                        </script>

                                    </fieldset>

                                    <footer>
                                    </footer>

                                </form>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->


                    </div>
                    <!-- end widget -->


                </article>
                <!-- WIDGET END -->

            </div>

            <!-- end row -->

        </section>
        <!-- end widget grid -->

    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->


<!-- PAGE FOOTER -->
<div class="page-footer">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <span class="txt-color-white">新闻管理系统</span>
        </div>

    </div>
</div>
<!-- END PAGE FOOTER -->

<!--================================================== -->

<script src="../static/js/libs/jquery-2.1.1.min.js"></script>

<script src="../static/js/libs/jquery-ui-1.10.3.min.js"></script>

<!-- IMPORTANT: APP CONFIG -->
<script src="../static/js/app.config.js"></script>

<!-- BOOTSTRAP JS -->
<script src="../static/js/bootstrap/bootstrap.min.js"></script>

<!-- JARVIS WIDGETS -->
<script src="../static/js/libs/jarvis.widget.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="../static/js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="../static/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="../static/js/plugin/select2/select2.min.js"></script>

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<!-- MAIN APP JS FILE -->
<script src="../static/js/app.min.js"></script>

<script>
    /*<![CDATA[*/
    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    $(function () {

        pageSetUp();

    })
    /*]]>*/
</script>

</body>

</html>



