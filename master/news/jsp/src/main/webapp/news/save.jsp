<%@ page import="com.news.bean.Constant" %>
<%@ page import="com.news.model.News" %>
<%@ page import="java.util.Date" %>
<%--
  Created by IntelliJ IDEA.
  User: maybe
  Date: 2015-10-14
  Time: 10:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="newsService" class="com.news.service.NewsService"></jsp:useBean>
<%
    News news = null;
    String newsIdStr = request.getParameter("news_id");
    if (newsIdStr != null && !newsIdStr.isEmpty()) {
        news = newsService.findNewsById(Long.parseLong(newsIdStr));
    }
    if (news == null) {
        news = new News();
        news.setNews_id(0l);
    }
    String source = request.getParameter("source");
    String statusStr = request.getParameter("status");
    Integer status = statusStr != null && statusStr.equals("on") ? Constant.CHANNEL_STATUS_ACTIVE : Constant.CHANNEL_STATUS_DETACHED;
    String channelIdStr = request.getParameter("channel_id");
    String title = request.getParameter("title");
    String content = request.getParameter("content");
    Long favorite_count = Long.parseLong(request.getParameter("favorite_count"));
    news.setSource(source);
    news.setTitle(title);
    news.setContent(content);
    news.setCreate_time(new Date().getTime());
    news.setStatus(status);
    news.setFavorite_count(favorite_count);
    news.setChannel_id(Long.parseLong(channelIdStr));


    newsService.saveNews(news);
    response.sendRedirect("list.jsp?channel_id=" + request.getParameter("channel_id"));
%>
<html>
<head>
    <title></title>
</head>
<body>
</body>
</html>
