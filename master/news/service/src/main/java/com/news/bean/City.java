package com.news.bean;

import java.util.List;

/**
 * Created by vitoling on 15/8/20.
 */
public class City {

    private Integer city_code;
    private String city_name;
    private List<String> districts;

    public List<String> getDistricts() {
        return districts;
    }

    public void setDistricts(List<String> districts) {
        this.districts = districts;
    }

    public Integer getCity_code() {
        return city_code;
    }

    public void setCity_code(Integer city_code) {
        this.city_code = city_code;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

}
