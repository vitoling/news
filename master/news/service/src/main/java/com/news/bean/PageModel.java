package com.news.bean;

/**
 * Created by lingzhiyuan.
 * Date : 15/9/2.
 * Time : 下午2:49.
 * Description:
 */
public class PageModel {

    // 一页的大小
    private Integer pageSize;
    // 上一页最后一项的id
    private Long lastId;
    // 查询的关键字
    private String queryKey;
    // 总项数
    private Long totalRows;
    // 总页数
    private Integer totalPages;
    // 当前页
    private Integer pageNum;

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public int getTotalPages() {
        return (int) ((totalRows % pageSize == 0) ? totalRows / pageSize
                : totalRows / pageSize + 1);
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Long getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(Long totalRows) {
        this.totalRows = totalRows;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Long getLastId() {
        return lastId;
    }

    public void setLastId(Long lastId) {
        this.lastId = lastId;
    }

    public String getQueryKey() {
        return queryKey;
    }

    public void setQueryKey(String queryKey) {
        this.queryKey = queryKey;
    }
}
