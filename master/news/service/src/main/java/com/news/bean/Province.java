package com.news.bean;

import java.util.List;

/**
 * Created by vitoling on 15/8/20.
 */
public class Province {

    private String province;
    private List<City> cities;

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

}
