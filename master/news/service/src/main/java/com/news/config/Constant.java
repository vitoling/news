package com.news.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@PropertySource("classpath:/config/config.properties")
public class Constant {

    public static final String DATE_FORMAT_YMDHM = "yyyy-MM-dd HH:mm";
    public static final String DATE_FORMAT_YMDHMS = "yyyy-MM-dd HH:mm:ss";

    public static final String DEFAULT_USERNAME = "admin";
    public static final String DEFAULT_PASSWORD = "123";

    public static final String Login_User_Cookie_Username_Name = "newsname";
    public static final String Login_User_Cookie_Status_Name = "newsstatus";

    public static final Integer USER_LOGIN_STATUS_ACTIVE = 10;
    public static final Integer USER_LOGIN_STATUS_DETACHED = 20;

    public static final Integer CHANNEL_STATUS_ACTIVE = 10;
    public static final Integer CHANNEL_STATUS_DETACHED = 20;

    public static final Integer NEWES_STATUS_ACTIVE = 10;
    public static final Integer NEWES_STATUS_DETACHED = 20;

    public static final String INDEX_PAGE_URL = "/";
    public static final String LOGIN_FALLBACK_PAGE_URL = "/gotoLogin";
    public static final String CHECK_LOGIN_PAGE_URL = "/user/login";


    public static final int COLUMN_SAVE_OPERATION_APPEND = 10;
    public static final int COLUMN_SAVE_OPERATION_UPDATE = 20;

    public final static int Channel_TYPE_HOME = 10;
    public final static int Channel_TYPE_NORMAL = 20;

    public final static int STATIS_STATUS_TRUE = 10;
    public final static int STATIS_STATUS_FALSE = 20;

    public final static int CHANNEL_COLUMN_ARTICLE_STATUS_ACTIVE = 10;
    public final static int CHANNEL_COLUMN_ARTICLE_STATUS_DISACTIVE = 20;

    public static final int COLUMN_TYPE_DEFAULT = 0;
    public static final int COLUMN_TYPE_NORMAL = 10;
    public static final long MAX_UPLOAD_IMAGE_SIZE = 10240000; // 10MB
    public final static int IMAGE_THUMBNAIL_WIDTH = 256;
    public final static int IMAGE_THUMBNAIL_HEIGHT = 256;
    public static String CMS_VERSION = "0.0.1";
    public static int SERVER_INFO_MEMORY_USED = 0;
    public static int SERVER_INFO_MEMORY_AVAILABLE = 0;
    public static int SERVER_INFO_MEMORY_TOTAL = 0;
    public static String SERVER_INFO_DESCRIPTION = "新闻后台管理系统";
    //文件上传到服务器的目录路径
    public static String UPLOAD_PATH;
    //上传文件后的URL地址前缀，也就是服务器地址
    public static String BASE_URL;
    @Autowired
    private Environment env;

    @PostConstruct
    public void init() {
        UPLOAD_PATH = env.getProperty("upload_path");
        BASE_URL = env.getProperty("base_url");
    }

}
