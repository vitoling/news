package com.news.dao;

import com.news.model.ChannelEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/8.
 * Time : 下午9:05.
 * Description:
 */

@Repository
public class ChannelDao extends BaseDao {

    public List<ChannelEntity> listChannelsByStatus(int status) {
        String hql = "from ChannelEntity where status<>? order by sequence asc";
        List channels;
        channels = hibernateTemplate.find(hql, status);
        if (channels == null || channels.size() == 0) {
            channels = null;
        }

        return channels;
    }

    public List<ChannelEntity> listChannels() {
        String hql = "from ChannelEntity order by sequence asc";
        List channels;
        channels = hibernateTemplate.find(hql);
        if (channels == null || channels.size() == 0) {
            channels = null;
        }

        return channels;
    }

    public ChannelEntity getLastChannelInSequence() {
        List list = hibernateTemplate.find("from ChannelEntity order by sequence asc");
        if (list != null && list.size() != 0) {
            return (ChannelEntity) list.get(list.size() - 1);
        }
        return null;
    }

}
