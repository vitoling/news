package com.news.dao;

import com.news.model.ColumnEntity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/6.
 * Time : 下午8:36.
 * Description:
 */

@Repository
public class ColumnDao extends BaseDao{

    public List<ColumnEntity> findColumnsByChannelIds(Set channelIds) {
        String hql = "from ColumnEntity where channel_id in (:ids) order by sequence asc";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setParameterList("ids",channelIds);
        return query.list();
    }


    public List findColumnsByChannelId(long channel_id) {
        String hql = "from ColumnEntity where channel_id = ? order by sequence asc";
        List<ColumnEntity> columns = (List<ColumnEntity>) hibernateTemplate.find(hql,channel_id);
        return (columns == null || columns.size() == 0)?null:columns;
    }


    public ColumnEntity getLastColumnInSequenceByChannelId(long channel_id){
        List list = hibernateTemplate.find("from ColumnEntity where channel_id=? order by sequence asc",channel_id);
        if (list != null && list.size() != 0){
            return (ColumnEntity) list.get(list.size()-1);
        }
        return null;
    }


}
