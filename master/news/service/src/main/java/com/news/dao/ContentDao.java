package com.news.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/7.
 * Time : 下午11:41.
 * Description:
 */

@Repository
public class ContentDao extends BaseDao {


    public List findByColumnId(long column_id) {

        String hql = "from ArticleEntity where column_id = ?";
        List result = hibernateTemplate.find(hql, column_id);
        if (result == null || result.size() == 0) {
            return null;
        }

        return result;
    }
}
