package com.news.dao;

import com.news.model.ImageEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/15.
 * Time : 下午11:16.
 * Description:
 */

@Repository
public class ImageDao extends BaseDao{

    public List listImages(int page_size,int page_num){

        Map result = new HashMap<>();
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(ImageEntity.class);
        criteria.addOrder(Order.desc("create_time"));

        criteria.setFirstResult(page_size*(page_num-1));
        criteria.setMaxResults(page_size);
        List<ImageEntity> images = criteria.list();
        return images;
    }



}
