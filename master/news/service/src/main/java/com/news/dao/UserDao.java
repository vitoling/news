package com.news.dao;

import com.news.model.UserEntity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Created by lingzhiyuan.
 * Date : 15/7/29.
 * Time : 下午3:56.
 * Description:
 */

@Repository("userDao")
public class UserDao extends BaseDao {


    /**
     * 根据手机获取用户信息
     */
    public UserEntity getUserByMobile(String mobile) {
        String hql = "from UserEntity where mobile=?";
        List<UserEntity> list = (List<UserEntity>) hibernateTemplate.find(hql, mobile);
        return list.size() == 0 ? null : list.get(0);
    }

    /**
     * 根据用户id列表获取所有用户
     */
    public List<UserEntity> getUsersByUserIds(Set userIds) {
        String hql = "from UserEntity where user_id in (:ids)";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setParameterList("ids", userIds);
        return query.list();
    }

    /**
     * 根据用户类型获取用户列表
     */
    public List<UserEntity> getUsersByUserType(int type, int page_size, long last_id) {

        String hql = "from UserEntity where user_type=? and user_id > ? order by user_id asc";
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setMaxResults(page_size);
        query.setInteger(0, type);
        query.setLong(1, last_id);
        return query.list();
    }
}
