package com.news.exception;

public enum BusinessError {
    //所有错误code，4位数字
    //用户帐户错误，1打头

    //拼车流程业务错误码
    ERROR_INVALID_IMAGE_TYPE(1001, "图片格式错误"),
    ERROR_INVALID_APP_TYPE(3012, "版本格式错误"),
    ERROR_INVALID_IMAGE(1002, "上传图片错误"),
    ERROR_IMAGE_SIZE_EXCEEDS_LIMIT(1005, "图片大小不符合要求"),
    ERROR_INVALID_APP(3011, "上传版本错误"),
    ERROR_FAIL_SEND_SMSS(1003, "短信发送失败"),
    ERROR_REQUEST_JSON(1004, "请求JSON格式错误"),

    ERROR_MOBILE_HAVE_REGISTER(2001, "该手机号已注册"),
    ERROR_NOT_REGISTER_MOBILE(2002, "您的手机号还未注册"),
    ERROR_NOT_FOUND_VERIFY_CODE(2003, "你还没有获取验证码或验证码已超时失效"),
    ERROR_THIS_VERIFY_CODE_NOT_USED_REGISTER(2004, "该验证码不是注册验证码"),
    ERROR_VERIFY_CODE(2005, "错误的手机验证码"),
    ERROR_MOBILE_OR_PASSWORD(2006, "错误手机号或者密码"),
    ERROR_PASSWORD(2007, "原密码错误"),
    ERROR_NULL_PASSWORD(2008, "密码不能为空"),
    ERROR_THIS_VERIFY_CODE_NOT_USED_RESET_PASSWORD(2009, "该验证码不是重置密码验证码"),
    ERROR_USER_DISABLED(2010, "账户已禁用"),
    ERROR_INVALID_THIRD_PARTY_ACCESS_INFO(2001, "第三方登录信息无效"),

    ERROR_MOBILE(2011, "错误的手机号码"),
    ERROR_SEND_SMS_FAIL(2012, "短信发送失败"),

    ERROR_PAY_TYPE_NOT_SUPPORTED(3001, "不支持的支付方式"),
    ERROR_USER_NOT_FOUND(9011, "用户不存在"),
    ERROR_USER_DUPLICATED(9012, "用户已存在"),
    ERROR_ROLE_NOT_FOUND(9013, "无此角色"),
    ERROR_ROLE_DUPLICATED(9014, "角色已存在"),
    ERROR_PERMISSION_NOT_FOUND(9015, "无此权限"),
    ERROR_PERMISSION_DUPLICATED(9016, "权限已存在"),

    ERROR_COLUMN_NOT_FOUND(3001, "栏目不存在"),
    ERROR_CHANNEL_NOT_FOUND(4001, "频道不存在"),
    ERROR_STATUS_NOT_FOUND(5001, "状态不存在"),
    ERROR_BAD_COLUMN_SAVE_OPERATION(3002, "栏目操作不存在"), ERROR_ARTICLE_NOT_FOUND(4002, "新闻不存在");


    private final int code;
    private final String description;

    private BusinessError(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code + ": " + description;
    }
}
