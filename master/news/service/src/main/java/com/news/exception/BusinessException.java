package com.news.exception;

public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = -4507058680879183136L;
    private BusinessError error;

    public BusinessException(BusinessError error) {
        this.error = error;
    }

    public BusinessError getError() {
        return error;
    }

    public void setError(BusinessError error) {
        this.error = error;
    }
}
