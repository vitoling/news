package com.news.http;

import java.util.List;

public class ListResponse extends BaseResponse
{
    private List<?> data;

    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }
}
