package com.news.http;

public class ObjectResponse extends BaseResponse
{
    private Object data;

    public Object getData()
    {
        return data;
    }

    public void setData(Object data)
    {
        this.data = data;
    }
}
