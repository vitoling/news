package com.news.http;

public class PagingResponse extends ListResponse
{
    private int has_more;

    public int getHas_more() {
        return has_more;
    }

    public void setHas_more(int has_more) {
        this.has_more = has_more;
    }
}
