package com.news.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.news.config.Constant;
import com.news.util.DateUtil;

import javax.persistence.*;
import java.text.ParseException;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/7.
 * Time : 下午11:39.
 * Description:
 * <p>
 * 文章实体类
 */

@Entity
@Table(name = "article")
public class ArticleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long article_id;
    private String title;
    private String content;
    private String source;
    private Long datetime;
    private Long column_id;
    private String url;
    private Integer status;

    @JsonIgnore
    @Transient
    private String dateTimeStr;

    public String getDateTimeStr() {

        try {
            return DateUtil.longToString(datetime, Constant.DATE_FORMAT_YMDHMS);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setDateTimeStr(String dateTimeStr) {
        this.dateTimeStr = dateTimeStr;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getColumn_id() {
        return column_id;
    }

    public void setColumn_id(Long column_id) {
        this.column_id = column_id;
    }

    public Long getArticle_id() {
        return article_id;
    }

    public void setArticle_id(Long article_id) {
        this.article_id = article_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Long getDatetime() {
        return datetime;
    }

    public void setDatetime(Long datetime) {
        this.datetime = datetime;
    }
}
