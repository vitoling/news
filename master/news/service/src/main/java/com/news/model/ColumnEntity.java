package com.news.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/6.
 * Time : 下午8:29.
 * Description:
 *
 * 栏目实体类
 *
 */

@Entity
@Table(name = "[column]")
public class ColumnEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long column_id;
    private Integer sequence;
    private String title;
    private Integer type;
    private Long channel_id;
    private Integer is_static;
    private String url;
    private Integer status;
    private String template;

    @Transient
    private List<ArticleEntity> articles;

    public List<ArticleEntity> getArticles() {
        return articles;
    }

    public void setArticles(List<ArticleEntity> newses) {
        this.articles = newses;
    }

    public Integer getIs_static() {
        return is_static;
    }

    public void setIs_static(Integer is_static) {
        this.is_static = is_static;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getChannel_id() {
        return channel_id;
    }

    public void setChannel_id(Long channel_id) {
        this.channel_id = channel_id;
    }

    public Long getColumn_id() {
        return column_id;
    }

    public void setColumn_id(Long column_id) {
        this.column_id = column_id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

}
