package com.news.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.news.config.Constant;
import com.news.util.DateUtil;

import javax.persistence.*;
import java.text.ParseException;

/**
 * Created by 凌志远.
 * Create Date : 2015/7/28.
 * Create TIME : 18:06.
 * Discription:
 */

@Entity
@Table(name = "image")
public class ImageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long image_id;
    private Long create_time;
    private String name;
    private String thumb;
    private String url;
    private String type;
    private Long size;

    @JsonIgnore
    @Transient
    private String createTimeStr;

    public String getCreateTimeStr() {
        try {
            return DateUtil.longToString(create_time, Constant.DATE_FORMAT_YMDHMS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setCreateTimeStr(String createTimeStr) {
        this.createTimeStr = createTimeStr;
    }

    public Long getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Long create_time) {
        this.create_time = create_time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getImage_id() {

        return image_id;
    }

    public void setImage_id(Long image_id) {
        this.image_id = image_id;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }


}
