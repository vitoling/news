package com.news.service;

import com.news.config.Constant;
import com.news.dao.ArticleDao;
import com.news.dao.ColumnDao;
import com.news.exception.BusinessError;
import com.news.exception.BusinessException;
import com.news.model.ArticleEntity;
import com.news.model.ColumnEntity;
import com.news.util.URLGenerator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/6.
 * Time : 下午8:37.
 * Description:
 */

@Service
@Transactional
public class ColumnService {

    @Resource
    private ColumnDao columnDao;
    @Resource
    private ArticleDao articleDao;

    /**
     * 保存栏目
     *  更新不修改所属频道
     * */
    public ColumnEntity saveColumn(long column_id,String title,int type,long channel_id,int operation){

        int sequence = 1;
        ColumnEntity column ;
        String url = URLGenerator.generate("/col");
        switch (operation){
            // 增加
            case Constant.COLUMN_SAVE_OPERATION_APPEND:
                column = new ColumnEntity();
                column.setChannel_id(channel_id);
                column.setStatus(Constant.CHANNEL_COLUMN_ARTICLE_STATUS_ACTIVE);
                ColumnEntity lastColumn = columnDao.getLastColumnInSequenceByChannelId(channel_id);
                if (lastColumn != null){
                    sequence = lastColumn.getSequence() + 1;
                }
                column.setSequence(sequence);
                break;
            // 更新
            case Constant.COLUMN_SAVE_OPERATION_UPDATE:
                column = columnDao.getById(ColumnEntity.class,column_id);
                if (column == null){
                    throw new BusinessException(BusinessError.ERROR_COLUMN_NOT_FOUND);
                }
                break;
            default:
                throw new BusinessException(BusinessError.ERROR_BAD_COLUMN_SAVE_OPERATION);
        }

        column.setType(type);
        column.setTitle(title);
        // 保存url时需要url以'/'开头
        column.setUrl(url.startsWith("/")?url:'/'+url);
        columnDao.saveOrUpdate(column);
        return column;
    }

    /**
     * 更新栏目所属频道
     * */
    public void updateChannelOfColumnByColumnIdChannelId(long column_id,long channel_id){
        ColumnEntity column = columnDao.getById(ColumnEntity.class,column_id);
        if (column == null){
            throw new BusinessException(BusinessError.ERROR_COLUMN_NOT_FOUND);
        }

        column.setChannel_id(channel_id);
        columnDao.saveOrUpdate(column);
        return;
    }

    /**
     * 删除特定的一个栏目
     * */
    public void deleteColumn(long column_id) {
        ColumnEntity column = columnDao.getById(ColumnEntity.class, column_id);
        List<ArticleEntity> articles;
        if (column != null){
            articles = articleDao.findArticleByColumnId(column_id);
            if (articles != null && articles.size() != 0){
                articleDao.batchDelete(articles);
            }
            columnDao.delete(column);
        }else {
            throw new BusinessException(BusinessError.ERROR_COLUMN_NOT_FOUND);
        }
    }

    /**
     * 获取一个特定的栏目
     * */
    public ColumnEntity getByColumnId(long columnd_id) {

        ColumnEntity column = columnDao.getById(ColumnEntity.class,columnd_id);
        if (column == null){
            throw new BusinessException(BusinessError.ERROR_COLUMN_NOT_FOUND);
        }
        return column;
    }

    /**
     * 获取一个特定的栏目
     * */
    public ColumnEntity getColumnAndArticlesByColumnId(long columnd_id) {

        ColumnEntity column = columnDao.getById(ColumnEntity.class,columnd_id);
        Map map = articleDao.findArticleByColumnId(columnd_id, 10, 1);
        List<ArticleEntity> articles = (List<ArticleEntity>) map.get("articles");
        if (column == null){
            throw new BusinessException(BusinessError.ERROR_COLUMN_NOT_FOUND);
        }
        if (articles != null && !articles.isEmpty()){
            column.setArticles(articles);
        }

        return column;
    }

    /**
     * 保存栏目模板
     * */
    public void saveTemplate(long column_id,String template) {
        ColumnEntity column = columnDao.getById(ColumnEntity.class,column_id);
        if (column == null){
            throw new BusinessException(BusinessError.ERROR_COLUMN_NOT_FOUND);
        }
        column.setTemplate(template);
        columnDao.saveOrUpdate(column);
        return;
    }

    /**
     * 根据频道ID更新栏目顺序
     * */
    public void updateColumnSequenceByChannelId(long channel_id,List<Long> column_ids) {

        List<ColumnEntity> columns = columnDao.findColumnsByChannelId(channel_id);
        int index = 0;

        if (columns != null && columns.size() != 0){
            for (long column_id : column_ids){
                Iterator<ColumnEntity> iterator  = columns.iterator();
                while (iterator.hasNext()){
                    ColumnEntity column = iterator.next();
                    if (column.getColumn_id() == column_id){
                        column.setSequence(++index);
                        iterator.remove();
                    }
                }
            }

            columnDao.batchSaveOrUpdate(columns);
        }
    }



}
