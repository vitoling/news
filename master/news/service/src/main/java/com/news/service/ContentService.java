package com.news.service;

import com.news.dao.ContentDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/7.
 * Time : 下午11:40.
 * Description:
 */

@Service
@Transactional
public class ContentService {

    @Resource
    private ContentDao contentDao;

    public List findByColumnId(long column_id) {

        return contentDao.findByColumnId(column_id);

    }

}
