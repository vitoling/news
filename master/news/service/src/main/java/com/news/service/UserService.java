package com.news.service;

import com.news.dao.UserDao;
import com.news.exception.BusinessError;
import com.news.exception.BusinessException;
import com.news.model.ImageEntity;
import com.news.model.UserEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

/**
 * Created by lingzhiyuan.
 * Date : 15/7/29.
 * Time : 下午2:57.
 * Description:
 */

@Service("userService")
@Transactional
public class UserService {

    @Resource(name = "userDao")
    private UserDao userDao;

    @Resource(name = "imageService")
    private ImageService imageService;


    /**
     * 登录类型分成手机号登录和第三方登录
     */
    public UserEntity login(int login_type,
                            String mobile,
                            String password,
                            String access_token) {

        UserEntity user = null;

        user = userDao.getUserByMobile(mobile);
        // 判断用户是否存在
        if (user == null) {
            throw new BusinessException(BusinessError.ERROR_USER_NOT_FOUND);
        }
        // 判断密码是否正确
        if (!user.getPassword().equals(password.trim())) {
            throw new BusinessException(BusinessError.ERROR_PASSWORD);
        }
        userDao.saveOrUpdate(user);

        return user;

    }

    // 保存一个新用户
    public UserEntity saveUser(long user_id, String mobile, String real_name, MultipartFile avatar) {
        UserEntity user = null;
        if (user_id != -1) {
            user = userDao.getById(UserEntity.class, user_id);
            if (user == null) {
                user = new UserEntity();
                user.setCreate_time(new Date().getTime());
            }
        } else {
            user = new UserEntity();
            user.setCreate_time(new Date().getTime());
        }

        user.setReal_name(real_name);
        user.setMobile(mobile);

        userDao.saveOrUpdate(user);

        return user;
    }

    public UserEntity getUserByUserId(long user_id) {
        return userDao.getById(UserEntity.class, user_id);
    }

    public UserEntity getUserByMobile(String mobile) {
        UserEntity user = userDao.getUserByMobile(mobile);
        if (user == null) {
            throw new BusinessException(BusinessError.ERROR_USER_NOT_FOUND);
        }
        return user;
    }

    public UserEntity saveOrUpdate(UserEntity user) {
        userDao.saveOrUpdate(user);
        return user;
    }

    /**
     * 新增手机号注册的用户
     */
    public UserEntity saveNewUser(String mobile, String password, Integer user_type) {

        UserEntity user = userDao.getUserByMobile(mobile);
        if (user == null) {
            user = new UserEntity();
            user.setPassword(password);
            user.setMobile(mobile);
            user.setCreate_time(new Date().getTime());
            user.setStatus(0);
        } else {
            throw new BusinessException(BusinessError.ERROR_USER_DUPLICATED);
        }

        userDao.saveOrUpdate(user);

        return user;

    }

    /**
     * 更新用户信息
     */
    public UserEntity updateUser(UserEntity user) {
        userDao.saveOrUpdate(user);
        return user;
    }


    /**
     * 更新用户头像
     */
    public UserEntity updateUserAvatar(String mobile, MultipartFile imageFile) {

        UserEntity user = userDao.getUserByMobile(mobile);
        ImageEntity image = null;
        String avatarUrl = null;
        String avatarThumbUrl = null;
        try {
            image = imageService.uploadImage(imageFile);
            avatarUrl = image.getUrl();
            avatarThumbUrl = image.getThumb();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        user.setAvatar(avatarUrl);
        user.setAvatar_thumb(avatarThumbUrl);
        userDao.saveOrUpdate(user);
        return user;
    }

    /**
     * 重置用户密码
     */
    public UserEntity resetUserPassword(String mobile, String new_password) {

        UserEntity user = userDao.getUserByMobile(mobile);
        // 判断是否存在该用户
        if (user == null) {
            throw new BusinessException(BusinessError.ERROR_USER_NOT_FOUND);
        }


        // 重置密码
        user.setPassword(new_password);
        userDao.saveOrUpdate(user);
        return user;
    }

    /**
     * 根据用户类型获取用户
     */
    public List<UserEntity> getUsersByUserType(int type, int page_size, long last_id) {
        return userDao.getUsersByUserType(type, page_size, last_id);
    }
}





