package com.news.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by vitoling on 15/8/25.
 */
public class DateUtil {

    public static Long stringToLong(String str,String dateFormat) throws ParseException {
        if (str == null){
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        Date time = sdf.parse(str);
        return time.getTime();
    }

    public static String longToString(Long time,String dateFormat) throws ParseException {
        if(time == null){
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        Date date = new Date(time);
        String dateString = sdf.format(date);
        return dateString;
    }

}
