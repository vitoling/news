package com.news.util;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.news.bean.Province;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/**
 * Created by lingzhiyuan.
 * Date : 15/8/11.
 * Time : 下午6:36.
 * Description:
 *
 * 从文件中获取城市区域信息
 *
 */
public class LocationUtil {

    private static Province[] provinces;

    static{
        if (provinces == null){
            try {

                InputStream is = LocationUtil.class.getResourceAsStream("");

                ObjectMapper mapper = new ObjectMapper();

                provinces = mapper.readValue(is, Province[].class);

            } catch (JsonMappingException e) {
                e.printStackTrace();
            } catch (JsonParseException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static List getLocations(){
        return Arrays.asList(provinces);
    }

}
