package com.news.util;

import java.util.UUID;

/**
 * Created by lingzhiyuan.
 * Date : 15/10/13.
 * Time : 下午10:04.
 * Description:
 */
public class URLGenerator {

    public static final String generate(String prefix) {
        if (!prefix.startsWith("/")) {
            prefix = "/" + prefix;
        }
        return prefix + UUID.randomUUID().toString().replace("-", "").substring(1, 5);
    }

}
